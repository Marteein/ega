extends ColorRect

func _ready():
	MusicController.play_music()

func _input(event):
	if(event is InputEventKey):
		go_to_mainscene()

func go_to_mainscene():
	get_tree().change_scene("res://scenes/MainScene.tscn")

func _on_AnimationPlayer_animation_finished(anim_name):
	go_to_mainscene()
	pass # Replace with function body.
